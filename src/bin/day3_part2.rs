// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

#[derive(Default, Clone, Copy)]
struct Bits {
    zeroes: u32,
    ones: u32,
}

#[derive(Debug)]
enum Mode {
    Oxygen,
    Co2,
}

impl Bits {
    fn add(&mut self, val: char) {
        match val {
            '0' => self.zeroes += 1,
            '1' => self.ones += 1,
            huh => panic!("Unexpected value: {}", huh),
        }
    }

    fn oxygen_bit(&self) -> char {
        if self.ones >= self.zeroes {
            '1'
        } else {
            '0'
        }
    }

    fn co2_bit(&self) -> char {
        if self.zeroes <= self.ones {
            '0'
        } else {
            '1'
        }
    }
}

fn char_at(input: &str, pos: usize) -> char {
    let chars: Vec<_> = input.chars().collect();
    chars[pos]
}

fn extract(input: Vec<String>, mode: Mode) -> String {
    let mut bit_position = 0;
    let mut input = input;
    loop {
        dbg!(&mode, &bit_position);
        dbg!(&input);
        let mut bit = Bits::default();
        for line in input.iter() {
            let char = char_at(line, bit_position);
            bit.add(char);
        }
        let keep = match mode {
            Mode::Oxygen => bit.oxygen_bit(),
            Mode::Co2 => bit.co2_bit(),
        };
        dbg!(&keep);
        let mut new = vec![];
        for line in input.into_iter() {
            let char = char_at(&line, bit_position);
            if char == keep {
                new.push(line);
            }
        }
        input = new;
        if input.len() == 1 {
            break;
        }
        bit_position += 1;
        if bit_position > 12 {
            panic!("Too high bits");
        }
    }
    input[0].to_string()
}

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day3.txt")?;
    let input: Vec<String> = input
        .trim()
        .to_string()
        .split('\n')
        .map(|s| s.to_string())
        .collect();
    let oxygen = extract(input.clone(), Mode::Oxygen);
    let co2 = extract(input, Mode::Co2);

    println!("oxygen={}, co2={}", oxygen, co2);
    // Sorry, convert them to decimal and multiply yourself
    Ok(())
}
