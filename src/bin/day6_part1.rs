// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

#[derive(Debug)]
struct Fish {
    timer: u32,
}

impl Fish {
    fn tick(&mut self) -> Option<Fish> {
        if self.timer == 0 {
            self.timer = 6;
            Some(Fish { timer: 8 })
        } else {
            self.timer -= 1;
            None
        }
    }
}

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day6.txt")?;
    let mut fishes: Vec<Fish> = input
        .trim()
        .split(',')
        .map(|val| Fish {
            timer: val.parse().unwrap(),
        })
        .collect();
    for _ in 1..=80 {
        let mut new_fishes = vec![];
        for fish in fishes.iter_mut() {
            if let Some(new) = fish.tick() {
                new_fishes.push(new);
            }
        }
        fishes.extend(new_fishes);
    }
    println!("Answer = {}", fishes.len());
    Ok(())
}
