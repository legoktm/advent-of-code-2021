// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

enum Command {
    Forward(u32),
    Down(u32),
    Up(u32),
}

impl Command {
    fn parse(input: &str) -> Self {
        let sp = input.split_once(' ').expect("no space in line");
        let value: u32 = sp.1.parse().expect("invalid value");
        match sp.0 {
            "forward" => Command::Forward(value),
            "down" => Command::Down(value),
            "up" => Command::Up(value),
            val => panic!("Unknown command: {}", val),
        }
    }
}

#[derive(Default)]
struct Position {
    horizontal: u32,
    depth: u32,
}

impl Position {
    fn apply(&mut self, command: Command) {
        match command {
            Command::Forward(val) => {
                self.horizontal += val;
            }
            Command::Down(val) => {
                self.depth += val;
            }
            Command::Up(val) => {
                self.depth -= val;
            }
        }
    }
}

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day2.txt")?;
    let mut position = Position::default();
    for line in input.trim().split('\n') {
        let command = Command::parse(line);
        position.apply(command);
    }

    println!("Answer: {}", position.horizontal * position.depth);
    Ok(())
}
