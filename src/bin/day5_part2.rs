// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::collections::HashMap;
use std::fs;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct Point {
    x: u32,
    y: u32,
}

impl Point {
    fn new(input: &str) -> Self {
        let (x, y) = input.split_once(',').unwrap();
        Self {
            x: x.parse().unwrap(),
            y: y.parse().unwrap(),
        }
    }
}

#[derive(Debug)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn new(input: &str) -> Self {
        let (start, end) = input.split_once(" -> ").unwrap();
        Self {
            start: Point::new(start),
            end: Point::new(end),
        }
    }

    fn points(&self) -> Vec<Point> {
        if self.start.x == self.end.x {
            let mut points = vec![];
            for y in range(self.start.y, self.end.y) {
                points.push(Point { x: self.start.x, y });
            }
            points
        } else if self.start.y == self.end.y {
            let mut points = vec![];
            for x in range(self.start.x, self.end.x) {
                points.push(Point { x, y: self.start.y });
            }
            points
        } else {
            let mut points = vec![];
            let xs = range(self.start.x, self.end.x);
            let ys = range(self.start.y, self.end.y);
            for (i, x) in xs.iter().enumerate() {
                points.push(Point { x: *x, y: ys[i] });
            }
            points
        }
    }
}

/// Does `a..=b` regardless of if a > b or b > a
fn range(a: u32, b: u32) -> Vec<u32> {
    if a > b {
        (b..=a).collect()
    } else if a < b {
        (a..=b).rev().collect()
    } else {
        panic!("a==b");
    }
}

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day5.txt")?;
    let mut lines = vec![];
    for line in input.trim().split('\n') {
        lines.push(Line::new(line));
    }
    let mut map: HashMap<Point, u32> = HashMap::new();
    for line in lines {
        for point in line.points() {
            map.entry(point).and_modify(|c| *c += 1).or_insert(1);
        }
    }
    let mut count = 0;
    for val in map.values() {
        if val > &1 {
            count += 1;
        }
    }
    println!("Answer = {}", count);
    Ok(())
}
