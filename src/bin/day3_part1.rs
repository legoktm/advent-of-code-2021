// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

#[derive(Default, Debug, Clone, Copy)]
struct Bits {
    zeroes: u32,
    ones: u32,
}

impl Bits {
    fn add(&mut self, val: char) {
        match val {
            '0' => self.zeroes += 1,
            '1' => self.ones += 1,
            huh => panic!("Unexpected value: {}", huh),
        }
    }

    fn common(&self) -> u32 {
        if self.zeroes > self.ones {
            0
        } else if self.ones > self.zeroes {
            1
        } else {
            unreachable!("tie?")
        }
    }

    fn least(&self) -> u32 {
        match self.common() {
            0 => 1,
            1 => 0,
            _ => unreachable!("tie not possible"),
        }
    }
}

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day3.txt")?;
    let mut all_bits = vec![Bits::default(); 12];
    for line in input.trim().split('\n') {
        for (i, char) in line.chars().enumerate() {
            all_bits[i].add(char);
        }
    }
    let mut gamma = vec![];
    let mut epsilon = vec![];
    for bit in all_bits {
        gamma.push(bit.common().to_string());
        epsilon.push(bit.least().to_string());
    }
    let gamma = gamma.join("");
    let epsilon = epsilon.join("");
    println!("gamma={}, epsilon={}", gamma, epsilon);
    // Sorry, convert them to decimal and multiply yourself
    Ok(())
}
