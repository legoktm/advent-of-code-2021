// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day1.txt")?;
    let mut iter = input.trim().split('\n').peekable();
    let mut count = 0;
    loop {
        let current: u32 = match iter.next() {
            Some(val) => val.parse()?,
            None => {
                break;
            }
        };
        if let Some(peeked) = iter.peek() {
            let peeked: u32 = peeked.parse()?;
            if peeked > current {
                count += 1;
            }
        }
    }
    println!("Answer: {}", count);
    Ok(())
}
