// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

#[derive(Debug)]
struct Grid {
    inner: Vec<Vec<u32>>,
}

impl Grid {
    fn new(input: &str) -> Self {
        let mut inner = vec![];
        for line in input.split('\n') {
            let mut row = vec![];
            for val in line.split_whitespace() {
                let val: u32 = val.parse().expect("invalid number");
                row.push(val);
            }
            assert_eq!(row.len(), 5);
            inner.push(row);
        }
        assert_eq!(inner.len(), 5);
        Self { inner }
    }

    fn is_finished(&self, selected: &[u32]) -> bool {
        // Walk through rows
        for row in &self.inner {
            // Every value in row is in the selected set
            if row.iter().all(|val| selected.contains(val)) {
                return true;
            }
        }
        // Now walk through columns
        for col_index in 0..=4 {
            if self
                .inner
                .iter()
                // Get the Xth value of each row
                .map(|row| &row[col_index])
                // Every value in the column is in the selected set
                .all(|val| selected.contains(val))
            {
                return true;
            }
        }
        false
    }

    fn unmarked(&self, selected: &[u32]) -> Vec<u32> {
        let mut unmarked = vec![];
        for row in &self.inner {
            for val in row {
                if !selected.contains(val) {
                    unmarked.push(*val);
                }
            }
        }
        unmarked
    }
}

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day4.txt")?;
    let mut chunks = input.trim().split("\n\n");
    let all_selected: Vec<u32> = chunks
        .next()
        .unwrap()
        .split(',')
        .map(|val| val.parse().expect("invalid number"))
        .collect();
    let mut grids = vec![];
    for chunk in chunks {
        grids.push(Grid::new(chunk));
    }
    // Now go through all_selected one by one
    let mut selected = vec![];
    for selection in &all_selected {
        selected.push(*selection);
        let mut not_finished = vec![];
        for grid in &grids {
            if !grid.is_finished(&selected) {
                not_finished.push(grid);
            }
        }
        if not_finished.len() == 1 {
            let grid = not_finished[0];
            // Now we need to finish this board
            for selection in &all_selected {
                selected.push(*selection);
                if grid.is_finished(&selected) {
                    let remaining: u32 =
                        not_finished[0].unmarked(&selected).iter().sum();
                    println!("Answer = {}", remaining * selection);
                    break;
                }
            }
        }
    }
    Ok(())
}
