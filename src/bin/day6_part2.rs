use std::collections::HashMap;
// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day6.txt")?;
    let mut map: HashMap<u64, u64> = HashMap::new();
    for val in input.trim().split(',') {
        let val: u64 = val.parse().unwrap();
        map.entry(val).and_modify(|c| *c += 1).or_insert(1);
    }
    for _ in 1..=256 {
        let vals: Vec<_> = map.clone().into_iter().collect();
        dbg!(&vals);
        map.clear();
        for (timer, number) in vals {
            if timer == 0 {
                // Reset the current # of fishes at timer=6
                map.entry(6).and_modify(|c| *c += number).or_insert(number);
                // And add the same amount of fishes at timer=8
                map.entry(8).and_modify(|c| *c += number).or_insert(number);
            } else {
                // Decrement this # of fishes at one less timer
                map.entry(timer - 1)
                    .and_modify(|c| *c += number)
                    .or_insert(number);
            }
        }
    }
    let count: u64 = map.values().sum();
    println!("Answer = {}", count);
    Ok(())
}
