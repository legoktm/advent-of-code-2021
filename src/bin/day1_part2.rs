// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use std::fs;

fn main() -> Result<()> {
    let input = fs::read_to_string("input/day1.txt")?;
    let values: Vec<u32> = input
        .trim()
        .split('\n')
        .map(|val| val.parse().expect("Invalid value"))
        .collect();
    let mut aggregated = vec![];
    for (i, value) in values.iter().enumerate() {
        let b = match values.get(i + 1) {
            Some(val) => val,
            None => {
                break;
            }
        };
        let c = match values.get(i + 2) {
            Some(val) => val,
            None => {
                break;
            }
        };
        aggregated.push(value + b + c);
    }

    let mut iter = aggregated.iter().peekable();
    let mut count = 0;
    loop {
        let current = match iter.next() {
            Some(val) => val,
            None => {
                break;
            }
        };
        if let Some(peeked) = iter.peek() {
            if peeked > &current {
                count += 1;
            }
        }
    }
    println!("Answer: {}", count);
    Ok(())
}
